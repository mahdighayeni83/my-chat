import React from "react";
// import '../bootstrap.min.css'
import './StyleHeme.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/css/bootstrap-grid.min.css'
import 'bootstrap/dist/js/bootstrap'
import 'bootstrap/dist/js/bootstrap.min'
import Audience from '../components/Audience'

const Home = () =>{
    return(
        <div className='div_main_Home'>
            <div className="div_main2__home

            ">
                <div className="div_main3___home
                row p-0 mx-0
                ">
                    <section className="section_left__contacts___home
                    col-12 col-md-4 col-xl-3
                    ">
                        <div className="div_mainSection__section___home">
                            <header className="header_search__section___home
                                row justify-content-center mt-3
                            ">
                                <div className="div_iconMenu_header__section___home
                                col-md-2 col d-flex justify-content-end align-items-center
                                ">
                                    <div className="div2__iconMenu_header__section___home">
                                        <div className="img_iconMenu_header__section___home"/>
                                    </div>
                                </div>
                                <div className="div_searchBox_header__section___home
                                col-md-10 col d-flex justify-content-center align-items-center
                                ">
                                    <input type="text" className="inp_search_header__section___home"/>
                                </div>
                            </header>
                            <div className="div_none__home"/>
                            <div className="div_contacts__bottomSearch__section___home">
                                <div className="div2__contacts__bottomSearch__section___home os-theme-light">
                                    <Audience/>
                                    <Audience/>
                                    <Audience/>
                                    <Audience/>
                                    <Audience/>
                                    <Audience/>
                                    <Audience/>
                                    <Audience/>
                                    <Audience/>
                                    <Audience/>
                                    <Audience/>
                                    <Audience/>
                                    <Audience/>
                                    <Audience/>

                                </div>
                            </div>
                        </div>
                    </section>

                    <div className="line_between__sectionAndMain___home col-auto"/>

                    <main className="main_chatroom__contents___home
                    col-md col-12 col-xl
                    ">

                    </main>

                </div>
            </div>
        </div>
    )
}
export default Home;