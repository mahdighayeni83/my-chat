import { red } from '@material-ui/core/colors';
import React, { useState } from 'react';
import "./StyleLogin.css"


const LoginPage = (props) => {
    let rep, rec;
    const [firstName, setFirstName] = useState();
    const [lastName, setLastName] = useState()
    const [myGender, setMyGender] = useState();

    const changFName = (event) => {
        setFirstName(event.target.value)
    }
    const changLName = (event) =>{
        setLastName(event.target.value)
    }
    const genderChang = (event) => {
        setMyGender(event.target.value)
    }
    const Submit = () =>{
        if (myGender !== "Man" && myGender!== "Female") {
            alert("Please inter your gender")
            rep = false
        }else rep = true

        if (!firstName || (firstName.length < 3 || lastName.length < 3)) {
            alert("   Please enter your name . " +
            "Note that your firstName and lastName must be more than '3' characters long ")
            rec = false
        }else rec = true

        if(rep === true && rec === true) {
            props.history.push({
                pathname: "Home",
                state: {
                    firstName,
                    myGender,
                    lastName
                }
            })
        }
    }
    return(
        <div className='main'>
            <div className='main2'>
                <div className='main3'>
                    <header className='head_name__login'>
                        <h1 className='h1_siteName__login'>
                            My Chat
                        </h1>
                        <h3>Welcome <span className='span_username_in_welcome__login' color={red}>{firstName}</span> to the My Chat</h3>
                    </header>
                    <main className='main_form__login'>
                        <div className='form__login'>
                            <div className='div_inputName__login'>
                                <input className='input_username__login' placeholder='Your firstName is ...'
                                       type='text' id='inputUser' value={firstName} onChange={changFName} />
                                <br/>
                                <input className='input_username__login' placeholder='Your lastName is ...'
                                       type='text' id='inputUser' value={lastName} onChange={changLName}/>
                                       <br/>
                                       <br/>

                            </div>
                            <select className='select_gender__login' onChange={genderChang}>
                                <option value="" label="country"/>
                                <option value="Man">Man</option>
                                <option value="Female">Female</option>
                            </select>

                            <br/><br/>
                            <div className='div_Specifications__login'>
                                <span className='Specifications__name__login'>{firstName} {lastName}</span>
                                <p>is</p>
                                <span className='Specifications__gender__login'>{myGender}</span>
                            </div>
                            <div className='div_btnNext__login'>
                                <button className='btn_submit__login' onClick={Submit} >Next</button>
                            </div>

                        </div>
                    </main>
                </div>
            </div>
        </div>
    );
};
export default LoginPage;