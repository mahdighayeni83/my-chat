import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min';
import 'bootstrap/dist/css/bootstrap-grid.min.css'
import 'bootstrap/dist/js/bootstrap'
import './Audience.css'

const Audience = () => {
    return(
        <div className="main_Audience">
            <div className="div_main__Audience">
                <div className="div2_main__Audience
                d-flex
                ">
                    <div className="div_imageProfile__Audience">
                        <div className="div2__imageProfile__Audience
                    d-flex justify-content-center align-items-center
                    ">
                            <div className="img_profile__Audience">
                                MG
                            </div>
                        </div>
                    </div>
                    <div className="div_nameAudience__Audience">
                        <div className="div2_nameAudience__Audience
                        d-flex justify-content-center align-items-center
                        ">
                            <h3 className="h3_fullName__Audience
                            m-0
                            ">
                                Mahdi Ghayeni
                            </h3>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    )
}
export default Audience;