import React from "react";
import './App.css';
import LoginPage from "./page/LoginPage"
import {BrowserRouter, Route , Switch} from 'react-router-dom'
import Home from "./page/Home";

function App() {
  return <BrowserRouter>
    <Switch>
      <Route exact path='/Home' component={Home} />
      <Route path='/login' component={LoginPage} />
      <Route component={LoginPage} />
    </Switch>
  </BrowserRouter>;
}

export default App;